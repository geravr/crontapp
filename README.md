# App crontab - Backbone
Reto con [Backbone Systems](https://www.backbonesystems.io/) basado en reto técnico proporcionado por Backbone
## ¿Cómo abordaste el problema?  
Inicialmente, pensé que sería algo muy simple de resolver, ya que consistía en realizar un CRUD a través del cliente.  
- ### Primeros pasos  
  Configuración de Next.js, Jest, react testing library.
  Inicié realizando la mayor parte de la UI sin funcionalidad, layout (header, main y footer), páginas de inicio, listado de recursos, detalles etc.  

- ### Aquí comienza lo entretenido  
  Cuando comencé con los formularios para el recurso de cronjobs, fué cuando me dí cuenta donde estaba realmente el reto. Fué un tema de mucho leer y pensar, cómo resolver de la mejor manera, el formato de cronjob en un formulario simple para el usuario.  

   - **Lo primero** fue pensar en como se lo iba a presentar al usuario, viendo el ejemplo dentro del reto y otras opciones que ví por internet, entendí que no podía simplificarlo mucho (por la cantidad de inputs), pero sabía que definitivamente un wizard multi-step podía ayudar, así que me dejé llevar desde el inicio por esa idea 
   - **Lo segundo** fue seleccionar un validador de formularios que me facilitara el trabajo, me fui por react hook form, decidí utilizar esta librería ya que es con la que me siento más cómodo usando por la experiencia pervia que tengo, además de que la lectura del código que genera, me es más satisfactorio (gusto personal).  
   - **Las cosas se pusieron más interesantes** cuando comencé a capturar la data del usuario, y tenía que entregarla a la api, en el formato correcto. Esto fué un reto que me hizo pensar y realizar refactor de mi código una y otra vez, hasta que porfin encontré la solución adecuada, crear traductores de formato de los inputs, al formato cron y viceversa. Parece simple, pero fué algo que me costó trabajo resolver en un inicio.  

## Plus  
 - #### Docker  
   - Decidí integrar docker al stack del proyecto, ya que es un plus que nos permite agilizar los entornos de desarrollo y producción, permitiendonos levantar ambos ambientes con unos pocos pasos.  
    Los beneficios que veo en el ambiente de desarrollo, son la mitigación del típico escenario donde la aplicación si funciona en local, pero en el equipo de otro colaborador no (incluso producción).
    El tiempo que demora un integrante nuevo en replicar los ambientes en su equipo, es considerablemente menor.  
 - ### Jest - React Testing Library - MSW (En proceso)  
   - Decidí integrar test de integración basadas en el usuario, con jest y react testing library, ya que pienso que son los test mejor balanceados en el frontend, ya que emulan gran parte de la interacción que tiene un usuario real con nuestra aplicación. Además se integrará Mock Service Worker, para realizar mockeado de la api en tiempo de testeo, de esta forma, los test se realizan como si estubiesen en un escenario real, pero sin la necesidad de estar consumiento la api real.  

## Tecnologías usadas  
- Next.js
- React Hook Form
- React bootstrap
- cronstrue
- propTypes
- Sass

## Iniciar el proyecto
#### Requisitos previos
 - Git
 - Docker
 - Docker compose

#### Primeros pasos
 - Deberás clonar el repositorio en tu equipo
 - Una vez clonado, deberás crear un archivo `.env.development` o `.env.production` (según sea el caso), y deberás establecer las variables de ambiente necesarias para el proyecto, puedes ver un ejemplo en el archivo `.env.example`
 - Abrir la temrinal y posicionarte en la ruta del proyecto  
#### Desarrollo
 - Deberás correr el siguiente comando para levantar el proyecto bajo el entorno de desarrollo  
  ```bash
  docker-compose -f docker-compose.dev.yml up --build
  ```  
#### Producción
 - Para levantar el proyecto en modo producción, deberás correr el siguiente comando  
  ```bash
  docker-compose -f docker-compose.prod.yml up --build
  ```

En ambos casos una vez que docker termine de construir y correr los contenedores, la aplicación estará disponible en [localhost:3000](http://localhost:3000/)


Happy hacking!
