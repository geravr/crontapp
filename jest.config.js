module.exports = {
  testPathIgnorePatterns: ["<rootDir>/.next/", "<rootDir>/node_modules/"],
  moduleNameMapper: {
    "\\.(css|less|scss|sass)$": "identity-obj-proxy",
    "@layout": ["<rootDir>/src/components/layout/Layout"],
    "@components(.*)$": "<rootDir>/src/components/$1",
    "@pages(.*)$": "<rootDir>/pages/$1",
    "@styleModules(.*)$": ["<rootDir>/styles/modules/$1"],
    "@styles(.*)$": ["<rootDir>/styles/$1"],
    "@arrayHelpers": ["<rootDir>/src/utils/arrayHelpers"],
    "@dateHelpers": ["<rootDir>/src/utils/dateHelpers"],
    "@utils(.*)$": ["<rootDir>/src/utils/$1"],
    "@api": ["<rootDir>/src/utils/api"],
    "@data(.*)$": ["<rootDir>/src/data/$1"],
    "@hooks(.*)$": ["<rootDir>/src/hooks/$1"],
    "@responses(.*)$": ["<rootDir>/src/mocks/responses/$1"],
    "@mock-server": ["<rootDir>/src/mocks/server"]
  },
  setupFilesAfterEnv: ["<rootDir>/setupTests.js"],
};
