import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

// Bootstrap
import { Row, Col } from "react-bootstrap";

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock, faCalendar } from "@fortawesome/free-solid-svg-icons";

// Components
import Seo from "@components/Seo";
import Layout from "@layout";
import CronjobList from "@components/CronjobList";
import DeleteModal from "@components/DeleteModal";

// utils
import { getCronjobs, deleteCronjob } from "@api";

export default function Home({ cronjobs }) {
  const router = useRouter();

  /************** States ***************/
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [cronjobToDelete, setCronjobToDelete] = useState(null);
  const [isDeleting, setIsDeleting] = useState(false);

  /************** Functions ***************/
  const refreshData = () => {
    router.replace(router.asPath);
  };

  const closeDeleteModal = () => {
    setIsDeleteModalVisible(false);
    setCronjobToDelete(null);
  };

  const openDeleteModal = (cronjobID) => {
    setIsDeleteModalVisible(true);
    setCronjobToDelete(cronjobID);
  };

  const deleteCron = async () => {
    setIsDeleting(true);
    await deleteCronjob(cronjobToDelete);
    setIsDeleting(false);
    closeDeleteModal();
    refreshData();
  };

  return (
    <>
      <Seo
        title='Crontab app'
        description='With this app you can make easly cron jobs'
      />
      <Layout>
        <DeleteModal
          isVisible={isDeleteModalVisible}
          handleClose={closeDeleteModal}
          handleOk={deleteCron}
          isDeleting={isDeleting}
        />
        <Row className='justify-content-between mb-2 mb-md-1'>
          <Col>
            <h1 className='mb-0'>Cronjob List</h1>
          </Col>
          <Col
            lg='auto'
            className='d-flex justify-md-content-end justify-md-content-start align-items-center'
          >
            <Link href='/cronjob/new' passHref>
              <a role='button' className='btn btn-secondary d-flex align-items-center'>
                <span className='fa-layers fa-fw fa-lg mr-1'>
                  <FontAwesomeIcon icon={faCalendar} />
                  <FontAwesomeIcon
                    icon={faClock}
                    transform='shrink-6 right-4 down-5'
                    className='text-secondary'
                  />
                </span>
                Add cronjob
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col>
            <CronjobList
              cronjobs={cronjobs}
              openDeleteModal={openDeleteModal}
            />
          </Col>
        </Row>
      </Layout>
    </>
  );
}

export async function getServerSideProps() {
  const { data: cronjobs } = await getCronjobs();
  return { props: { cronjobs } };
}
