// Components
import Seo from "@components/Seo";
import Layout from "@layout";

// utils
import { getCronjobByID, getWorkflowByID } from "@api";

// Bootstrap
import { Container, Row, Col, Card } from "react-bootstrap";

// Cronstrue
import cronstrue from "cronstrue";

const CronjobDetailPage = (props) => {
  const { cronjob, workflow } = props;
  const translatedSchedule = cronstrue.toString(cronjob.scheduling);

  return (
    <>
      <Seo title={cronjob.name} description={cronjob.description} />
      <Layout>
        <Row>
          <Col>
            <h1>Cronjob detail</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card border='primary' as={Container}>
              <Card.Header
                as={Row}
                className='bg-primary justify-content-between align-items-center rounded-top text-light'
              >
                <span>
                  <strong>Schedule</strong>
                  <p className='my-0'>{translatedSchedule}</p>
                </span>
              </Card.Header>
              <Row className='flex-column px-3 pt-3'>
                <span className='d-flex mb-3'>
                  <h5 className='my-0 mr-2'>Name</h5>
                  <p className='my-0'>{cronjob.name}</p>
                </span>
                <h5>Description</h5>
                <p>{cronjob.description}</p>
              </Row>
              <hr />
              <Row className='justify-content-between px-3 pb-2'>
                <small className='text-muted'>
                  <strong>Last update:</strong> {cronjob.updated_at}
                </small>
                <small className='text-muted'>
                  <strong>Workflow:</strong> {workflow.name}
                </small>
              </Row>
            </Card>
          </Col>
        </Row>
      </Layout>
    </>
  );
};

export default CronjobDetailPage;

export async function getServerSideProps({ params }) {
  const { data: cronjob } = await getCronjobByID(params.id);
  const { data: workflow } = await getWorkflowByID(cronjob.workflow_id);

  return { props: { cronjob, workflow } };
}
