import { useState, useEffect } from "react";
import { useRouter } from "next/router";

// Bootstrap
import { Row, Col } from "react-bootstrap";

// Components
import Seo from "@components/Seo";
import Layout from "@layout";
import CronjobForm from "@components/forms/CronjobForm";
import Spinner from "@components/Spinner";
import Feedback from "@components/Feedback";

// api
import { updateCronjob } from "@api";

// Api
import { getCronjobByID } from "@api";

const EditCronPage = () => {
  /*************** States ***************/
  const [isSuccess, setIsSuccess] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState({
    hasError: false,
    title: "",
    message: "",
  });

  const router = useRouter();
  const { id } = router.query;

  /*************** Functions ***************/
  const onSubmit = async (data) => {
    setIsLoading(true);
    data.scheduling = data.scheduling.join(" ");
    try {
      await updateCronjob(id, data);
      setIsSuccess(true);
    } catch (error) {
      setError({
        hasError: true,
        message: error?.response?.data?.message,
        title: error?.response?.data?.errors?.scheduling,
      });
    } finally {
      setIsLoading(false);
    }
  };

  const clearFeedback = () => {
    setError({
      hasError: false,
      title: "",
      message: "",
    });
    setIsSuccess(false);
    router.push("/");
  };

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await getCronjobByID(id);
      setData({
        ...data,
      });
    };
    if (id) {
      fetchData();
    }
  }, [id]);

  return (
    <>
      <Seo
        title='New cronjob'
        description='In this section you can create a cronjob'
      />
      <Layout>
        <Row className='px-2'>
          <Col sm='12'>
            <h1>Edit cronjob</h1>
          </Col>
          <Col sm='12'>
            {data ? (
              <CronjobForm
                preloadedData={data}
                onSubmit={onSubmit}
                isLoading={isLoading}
              />
            ) : (
              <Spinner />
            )}
          </Col>
        </Row>
        <Feedback
          isVisible={isSuccess}
          message='Cronjob updated successfully'
          onClose={clearFeedback}
          type='success'
          intervalClose={1000}
        />
        <Feedback
          isVisible={error.hasError}
          title={error.title}
          message={error.message}
          onClose={clearFeedback}
          type='danger'
        />
      </Layout>
    </>
  );
};

export default EditCronPage;
