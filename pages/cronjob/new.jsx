import { useState } from "react";
import { useRouter } from "next/router";

// Bootstrap
import { Row, Col } from "react-bootstrap";

// Components
import Seo from "@components/Seo";
import Layout from "@layout";
import CronjobForm from "@components/forms/CronjobForm";
import Feedback from "@components/Feedback";

// api
import { createCronjob } from "@api";

const NewCronjobPage = () => {
  /*************** States ***************/
  const [isSuccess, setIsSuccess] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState({
    hasError: false,
    title: "",
    message: "",
  });
  const router = useRouter();

  /*************** Functions ***************/
  const onSubmit = async (data) => {
    setIsLoading(true);
    data.scheduling = data.scheduling.join(" ");
    try {
      await createCronjob(data);
      setIsSuccess(true);
    } catch (error) {
      setError({
        hasError: true,
        message: error?.response?.data?.message,
        title: error?.response?.data?.errors?.scheduling,
      });
    } finally {
      setIsLoading(false);
    }
  };

  const clearFeedback = () => {
    setError({
      hasError: false,
      title: "",
      message: "",
    });
    setIsSuccess(false);
    router.push("/");
  };

  return (
    <>
      <Seo
        title='New cronjob'
        description='In this section you can create a cronjob'
      />
      <Layout>
        <Row className='px-2'>
          <Col sm='12'>
            <h1>New cronjob</h1>
          </Col>
          <Col sm='12'>
            <CronjobForm onSubmit={onSubmit} isLoading={isLoading} />
          </Col>
        </Row>
        <Feedback
          isVisible={isSuccess}
          message='Cronjob created successfully'
          onClose={clearFeedback}
          type='success'
          intervalClose={1000}
        />
        <Feedback
          isVisible={error.hasError}
          title={error.title}
          message={error.message}
          onClose={clearFeedback}
          type='danger'
        />
      </Layout>
    </>
  );
};

export default NewCronjobPage;
