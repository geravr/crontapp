import { useState, useEffect } from "react";
import { useRouter } from "next/router";

// Bootstrap
import { Row, Col, Card } from "react-bootstrap";

// Components
import Seo from "@components/Seo";
import Layout from "@layout";
import Workflowform from "@components/forms/WorkflowForm";
import Feedback from "@components/Feedback";
import Spinner from "@components/Spinner";

// Api
import { updateWorkflow, getWorkflowByID } from "@api";

export default function Home() {
  /*************** States ***************/
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [error, setError] = useState({
    hasError: false,
    title: "",
    message: "",
  });

  const router = useRouter();
  const { id } = router.query;

  /*************** Functions ***************/
  const clearFeedback = () => {
    setError({
      hasError: false,
      title: "",
      message: "",
    });
    setIsSuccess(false);
    router.push("/workflow");
  };

  const goToWorkflowPage = () => {
    router.push("/workflow");
  };

  const onSubmit = async (data) => {
    try {
      setIsLoading(true);
      await updateWorkflow(id, data);
      setIsSuccess(true);
    } catch (error) {
      setError({
        hasError: true,
        message: error?.response?.data?.message,
        title: error?.response?.data?.errors,
      });
    } finally {
      setIsLoading(false);
    }
  };

  /*************** Lifecycle ***************/
  useEffect(() => {
    if (id) {
      const fetchData = async () => {
        const { data } = await getWorkflowByID(id);
        setData({ ...data });
      };
      fetchData();
    }
  }, [id]);

  return (
    <>
      <Seo
        title='Crontab app'
        description='With this app you can make easly workflows'
      />
      <Layout>
        <Row className='justify-content-between mb-2 mb-md-1'>
          <Col>
            <h1 className='mb-0'>New workflow</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            {data ? (
              <Workflowform
                onSubmit={onSubmit}
                onCancel={goToWorkflowPage}
                isLoading={isLoading}
                preloadedData={data}
              />
            ) : (
              <Spinner />
            )}
          </Col>
        </Row>
        <Feedback
          isVisible={isSuccess || error.hasError}
          title={error.hasError ? error.title : undefined}
          message={isSuccess ? `Workflow updated successfully` : error.message}
          onClose={clearFeedback}
          type={isSuccess ? "success" : error.hasError ? "danger" : undefined}
          intervalClose={2000}
        />
      </Layout>
    </>
  );
}
