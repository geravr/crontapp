import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

// Bootstrap
import { Row, Col } from "react-bootstrap";

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStream } from "@fortawesome/free-solid-svg-icons";

// Components
import Seo from "@components/Seo";
import Layout from "@layout";
import WorkflowList from "@components/WorkflowList";
import DeleteModal from "@components/DeleteModal";
import Feedback from "@components/Feedback";

// utils
import { getWorkflows, deleteWorkflow } from "@api";

export default function WorkflowPage({ workflows }) {
  const router = useRouter();

  /************** States ***************/
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [workflowToDelete, setWorkflowToDelete] = useState(null);
  const [isDeleting, setIsDeleting] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [error, setError] = useState({
    hasError: false,
    title: "",
    message: "",
  });

  /************** Functions ***************/
  const refreshData = () => {
    router.replace(router.asPath);
  };

  const closeDeleteModal = () => {
    setIsDeleteModalVisible(false);
    setWorkflowToDelete(null);
  };

  const openDeleteModal = (cronjobID) => {
    setIsDeleteModalVisible(true);
    setWorkflowToDelete(cronjobID);
  };

  const clearFeedback = () => {
    setError({
      hasError: false,
      title: "",
      message: "",
    });
    setIsSuccess(false);
  };

  const deleteWork = async () => {
    try {
      setIsDeleting(true);
      await deleteWorkflow(workflowToDelete);
      setIsSuccess(true);
      refreshData();
    } catch (error) {
      setError({
        ...error,
        hasError: true,
        message: error?.response?.data?.message,
      });
    } finally {
      setIsDeleting(false);
      closeDeleteModal();
    }
  };

  return (
    <>
      <Seo
        title='Crontab app'
        description='With this app you can make easly workflows'
      />
      <Layout>
        <DeleteModal
          title='Do you want to delete this workflow?'
          isVisible={isDeleteModalVisible}
          handleClose={closeDeleteModal}
          handleOk={deleteWork}
          isDeleting={isDeleting}
        />
        <Row className='justify-content-between mb-2 mb-md-1'>
          <Col>
            <h1 className='mb-0'>Workflow List</h1>
          </Col>
          <Col
            lg='auto'
            className='d-flex justify-md-content-end justify-md-content-start align-items-center'
          >
            <Link href='/workflow/new' passHref>
              <a role='button' className='btn btn-secondary d-flex align-items-center'>
                <span className='fa-layers fa-fw fa-lg mr-1'>
                  <FontAwesomeIcon icon={faStream} />
                </span>
                Add workflow
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col>
            <WorkflowList
              workflows={workflows}
              openDeleteModal={openDeleteModal}
            />
          </Col>
        </Row>
        <Feedback
          isVisible={isSuccess || error.hasError}
          title={error.hasError ? error.title : undefined}
          message={isSuccess ? `Workflow deleted successfully` : error.message}
          onClose={clearFeedback}
          type={isSuccess ? "success" : error.hasError ? "danger" : undefined}
          intervalClose={2000}
        />
      </Layout>
    </>
  );
}

export async function getServerSideProps() {
  const { data: workflows } = await getWorkflows();

  return { props: { workflows } };
}
