import { useState } from "react";
import { useRouter } from "next/router";

// Bootstrap
import { Row, Col, Card } from "react-bootstrap";

// Components
import Seo from "@components/Seo";
import Layout from "@layout";
import Workflowform from "@components/forms/WorkflowForm";
import Feedback from "@components/Feedback";

// Api
import { createWorkflow } from "@api";

export default function Home() {
  const router = useRouter();
  /************** States ***************/
  const [isSuccess, setIsSuccess] = useState(false);
  const [error, setError] = useState({
    hasError: false,
    title: "",
    message: "",
  });

  /************** Functions ***************/
  const clearFeedback = () => {
    setError({
      hasError: false,
      title: "",
      message: "",
    });
    setIsSuccess(false);
    router.push("/workflow");
  };

  const goToWorkflowPage = () => {
    router.push("/workflow");
  };

  const onSubmit = async (data) => {
    try {
      await createWorkflow(data);
      setIsSuccess(true);
    } catch (error) {
      setError({
        hasError: true,
        message: error?.response?.data?.message,
        title: error?.response?.data?.errors,
      });
    }
  };

  return (
    <>
      <Seo
        title='Crontab app'
        description='With this app you can make easly workflows'
      />
      <Layout>
        <Row className='justify-content-between mb-2 mb-md-1'>
          <Col>
            <h1 className='mb-0'>New workflow</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <Workflowform onSubmit={onSubmit} onCancel={goToWorkflowPage} />
          </Col>
        </Row>
        <Feedback
          isVisible={isSuccess || error.hasError}
          title={error.hasError && error.title}
          message={isSuccess ? `Workflow created successfully` : error.message}
          onClose={clearFeedback}
          type={isSuccess ? "success" : error.hasError ? "danger" : undefined}
          intervalClose={2000}
        />
      </Layout>
    </>
  );
}
