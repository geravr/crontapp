import PropTypes from "prop-types";

// Bootstrap
import { Card } from "react-bootstrap";

// Components
import WizardHeader from "@components/wizard/WizardHeader";
import WizardBody from "@components/wizard/WizardBody";
import WizardFooter from "@components/wizard/WizardFooter";

const Wizard = (props) => {
  const { children, isDisabled = false } = props;

  return (
    <Card
      body
      border='primary'
      className={`row ${isDisabled ? "disabled-element" : ""}`}
    >
      {children}
    </Card>
  );
};

Wizard.Header = WizardHeader;
Wizard.Body = WizardBody;
Wizard.Footer = WizardFooter;

Wizard.propTypes = {
  children: PropTypes.node,
  isDisabled: PropTypes.bool,
};

export default Wizard;
