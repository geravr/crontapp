import PropTypes from "prop-types";

// Bootstrap
import { Row, Col, Button } from "react-bootstrap";

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
} from "@fortawesome/free-solid-svg-icons";

const WizardFooter = (props) => {
  const {
    currentStep,
    steps,
    handlePrevious,
    handleNext,
    isUpdate,
    onCancel,
  } = props;
  return (
    <Col xs={12}>
      <Row className='justify-content-between'>
        <Col
          xs={12}
          md={6}
          className='d-flex justify-content-center justify-content-md-start mb-5 mb-md-0'
        >
          <Button
            variant='outline-secondary'
            onClick={handlePrevious}
            disabled={currentStep === 1}
            className='mr-1'
          >
            <FontAwesomeIcon icon={faChevronLeft} /> Prev
          </Button>
          <Button
            variant='outline-secondary'
            onClick={handleNext}
            disabled={currentStep === steps.length}
            className='ml-1'
          >
            Next <FontAwesomeIcon icon={faChevronRight} />{" "}
          </Button>
        </Col>
        <Col
          xs={12}
          md={6}
          className='d-flex justify-content-between  justify-content-md-end'
        >
          <Button variant='outline-warning' className='mr-2' onClick={onCancel}>
            Cancel
          </Button>
          <Button form='cronForm' type='submit'>
            {isUpdate ? "Update cron" : "Create Cron"}
          </Button>
        </Col>
      </Row>
    </Col>
  );
};

WizardFooter.propTypes = {
  currentStep: PropTypes.number,
  steps: PropTypes.arrayOf(PropTypes.object),
  handlePrevious: PropTypes.func,
  handleNext: PropTypes.func,
  isUpdate: PropTypes.bool,
  onCancel: PropTypes.func,
};

export default WizardFooter;
