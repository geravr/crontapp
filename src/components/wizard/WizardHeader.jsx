import PropTypes from "prop-types";

// Bootstrap
import { Col, ButtonGroup, Button } from "react-bootstrap";

const WizardHeader = (props) => {
  const { steps, currentStep, handleClickStep, subtitle } = props;
  return (
    <Col
      xs={12}
      className='d-flex flex-column justify-content-center align-items-center position-relative'
    >
      <p
        className='position-absolute mr-0 mt-n3 mt-md-n1'
        style={{ right: "0", top: "0" }}
      >{`Step ${currentStep} of ${steps.length}`}</p>
      <h3>{steps[currentStep - 1].name}</h3>
      <ButtonGroup>
        {steps.slice(1, steps.length).map((step) => (
          <Button
            variant={currentStep === step.id ? "primary" : "outline-primary"}
            onClick={() => handleClickStep(step.id)}
            key={step.id}
          >
            {step.name[0]}
          </Button>
        ))}
      </ButtonGroup>
      <p className='mt-1 mb-n3 my-mb-0'>{subtitle}</p>
    </Col>
  );
};

WizardHeader.propTypes = {
  steps: PropTypes.arrayOf(PropTypes.object),
  currentStep: PropTypes.number,
  handleClickStep: PropTypes.func,
  subtitle: PropTypes.string,
};

export default WizardHeader;
