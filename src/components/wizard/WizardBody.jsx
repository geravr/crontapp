import PropTypes from "prop-types";

// Bootstrap
import { Col } from "react-bootstrap";

const WizardBody = ({ children }) => {
  return (
    <Col xs={12} className='my-4'>
      {children}
    </Col>
  );
};

WizardBody.propTypes = {
  children: PropTypes.node,
};

export default WizardBody;
