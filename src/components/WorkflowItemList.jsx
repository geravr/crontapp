import Link from "next/link";
import PropTypes from "prop-types";

// Bootstrap
import { Button } from "react-bootstrap";

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

const WorkflowItemList = ({ workflow, openDeleteModal }) => {
  /************** Functions ***************/
  const getExcerpt = (content) => {
    if (content.length <= 50) {
      return content;
    }
    return content.substring(0, 50) + "...";
  };

  const excerpt = getExcerpt(workflow.description);

  return (
    <tr>
      <td style={{ minWidth: "15vw" }}>{workflow.name}</td>
      <td style={{ minWidth: "20vw" }}>{excerpt}</td>
      <td>
        <div className='d-flex justify-content-center'>
          <Link href={`/workflow/edit/${workflow.id}`} passHref>
            <a
              className='btn btn-outline-secondary mx-1'
              data-testid='edit-button'
            >
              <FontAwesomeIcon icon={faPen} />
            </a>
          </Link>
          <Button
            data-testid='delete-button'
            variant='outline-danger'
            onClick={() => openDeleteModal(workflow.id)}
          >
            <FontAwesomeIcon icon={faTrashAlt} />
          </Button>
        </div>
      </td>
    </tr>
  );
};

WorkflowItemList.propTypes = {
  workflow: PropTypes.object.isRequired,
  openDeleteModal: PropTypes.func,
};

export default WorkflowItemList;
