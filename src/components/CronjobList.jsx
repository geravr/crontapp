import PropTypes from "prop-types";

// components
import List from "@components/list/List";
import CronjobItemList from "@components/CronjobItemList";

const CronjobList = ({ cronjobs, openDeleteModal }) => {
  return (
    <List>
      <List.Header>
        <th>Name</th>
        <th>Description</th>
        <th>Description</th>
        <th>Actions</th>
      </List.Header>
      <List.Body>
        {cronjobs.map((cronjob) => (
          <CronjobItemList
            key={cronjob.id}
            cronjob={cronjob}
            openDeleteModal={openDeleteModal}
          />
        ))}
      </List.Body>
    </List>
  );
};

CronjobItemList.propTypes = {
  cronjobs:  PropTypes.arrayOf(PropTypes.object),
  openDeleteModal: PropTypes.func,
};

export default CronjobList;
