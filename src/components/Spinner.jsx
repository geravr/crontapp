import style from "@styleModules/Spinner.module.scss";

const Spinner = () => {
  return <div className={style.spinner}>Loading...</div>;
};

export default Spinner;
