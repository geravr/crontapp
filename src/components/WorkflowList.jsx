import PropTypes from "prop-types";

// components
import List from "@components/list/List";
import WorkflowItemList from "@components/WorkflowItemList";

const WorkflowList = ({ workflows, openDeleteModal }) => {
  return (
    <List>
      <List.Header>
        <th>Name</th>
        <th>Description</th>
        <th className='text-center'>Actions</th>
      </List.Header>
      <List.Body>
        {workflows.map((workflow) => (
          <WorkflowItemList
            key={workflow.id}
            workflow={workflow}
            openDeleteModal={openDeleteModal}
          />
        ))}
      </List.Body>
    </List>
  );
};

WorkflowList.propTypes = {
  workflows: PropTypes.arrayOf(PropTypes.object),
  openDeleteModal: PropTypes.func,
};

export default WorkflowList;
