import PropTypes from 'prop-types';

// Bootstrap
import { Modal, Button, Spinner } from "react-bootstrap";

const DeleteModal = (props) => {
  const { isVisible, handleClose, handleOk, isDeleting, title = 'Do you want to delete this cron job?' } = props;
  return (
    <Modal show={isVisible} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>Once deleted, it cannot be recovered</Modal.Body>
      <Modal.Footer>
        <Button variant='outline-secondary' onClick={handleClose}>
          Cancel
        </Button>
        <Button variant='danger' onClick={handleOk} disabled={isDeleting}>
          {isDeleting && (
            <Spinner
              as='span'
              animation='border'
              size='sm'
              role='status'
              aria-hidden='true'
            />
          )}{" "}
          Yes, delete
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

DeleteModal.propTypes = {
  isVisible: PropTypes.bool,
  handleClose: PropTypes.func,
  handleOk: PropTypes.func,
  isDeleting: PropTypes.bool,
  title: PropTypes.string,
};

export default DeleteModal;
