import { useEffect } from "react";
import PropTypes from "prop-types";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Hook Form
import { useForm } from "react-hook-form";
import { useWatch } from "react-hook-form";

// Helpers
import { getNumbersInArray } from "@arrayHelpers";
import {
  getTranslatedValuesToCronFormat,
  getTranslatedValuesFromCronFormat,
} from "@utils/translators";

const HoursForm = (props) => {
  const { isVisible, hookForm, preloadedData } = props;
  const hoursArray = getNumbersInArray(24);

  const initialData = {
    periodicityType: "choosedValues",
    choosedValues: ["0"],
    ...(preloadedData &&
      getTranslatedValuesFromCronFormat(preloadedData, "Hours")),
  };

  // Global form
  const { setValue } = hookForm;

  // Local form
  const {
    control,
    register,
    formState: { errors },
  } = useForm({
    defaultValues: initialData,
  });

  const hoursWatch = useWatch({ control });

  const { periodicityType, choosedValues } = hoursWatch;

  /*************** Lifecycle ***************/
  useEffect(() => {
    // Translate to cron format
    const hours = getTranslatedValuesToCronFormat(hoursWatch);
    // Set to global form
    setValue("scheduling[2]", hours);
  }, [hoursWatch]);

  return (
    <div className={isVisible ? "" : "d-none"}>
      <Form.Row>
        <Form.Group as={Col}>
          <Form.Label>Select a periodicity</Form.Label>
          <Form.Control
            as='select'
            defaultValue={periodicityType}
            {...register("periodicityType")}
          >
            <option value={"everyHours"}>Every hour</option>
            <option value={"intervalValues"}>
              Every "x" hour(s) starting at hour "x"
            </option>
            <option value={"choosedValues"}>
              Specific hour (choose one or many)
            </option>
            <option value={"betweenValues"}>
              Every hour between hour "x" and hour "x"
            </option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
      <Form.Row>
        {/* Interval hours */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "intervalValues" ? "" : "d-none"
          }`}
        >
          Every
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='m-1 w-auto'
            {...register("intervalValues.eachValue")}
          >
            {hoursArray.map((hour) => {
              hour = parseInt(hour) + 1;
              return (
                <option value={hour} key={hour}>
                  {hour}
                </option>
              );
            })}
          </Form.Control>
          <div className='mr-5 mr-md-1'>hour(s)</div> starting at hour
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='m-1 w-auto'
            {...register("intervalValues.startValue")}
          >
            {hoursArray.map((hour) => (
              <option value={hour} key={hour}>
                {hour}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        {/* Choose hours */}
        <Form.Group
          as={Col}
          className={`${periodicityType == "choosedValues" ? "" : "d-none"}`}
        >
          <Row>
            {hoursArray.map((hour) => (
              <Col xs={3} sm={2} lg={1} key={hour}>
                <Form.Check
                  custom
                  type='checkbox'
                  id={`check-hour-${hour}`}
                  label={hour}
                  value={hour}
                  defaultChecked={choosedValues?.includes(hour)}
                  {...register("choosedValues", {
                    required: {
                      value: true,
                      message: "You must select at least one option",
                    },
                  })}
                />
              </Col>
            ))}
          </Row>
          <p className='mb-0 mt-2 text-danger'>
            {errors?.hours?.choosedValues?.message}
          </p>
        </Form.Group>

        {/* Between hours of a day */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "betweenValues" ? "" : "d-none"
          }`}
        >
          Every hour between hour
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='mx-1'
            {...register(`betweenValues.valueOne`)}
          >
            {hoursArray.map((hour) => (
              <option value={hour} key={hour}>
                {hour}
              </option>
            ))}
          </Form.Control>
          and hour
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='mx-1'
            {...register(`betweenValues.valueTwo`)}
          >
            {hoursArray.map((hour) => (
              <option value={hour} key={hour}>
                {hour}
              </option>
            ))}
          </Form.Control>
        </Form.Group>
      </Form.Row>
    </div>
  );
};

HoursForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
  preloadedData: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

export default HoursForm;
