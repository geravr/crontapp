import PropTypes from "prop-types";

// React hook form
import { useForm } from "react-hook-form";

// Bootstrap
import { Form, Col, Button, Card } from "react-bootstrap";

const WorkflowForm = ({ preloadedData, onSubmit, onCancel }) => {
  const {
    handleSubmit,
    formState: { errors: formErrors, isSubmitSuccessful, isSubmitting },
    register,
  } = useForm({ mode: "onBlur", defaultValues: preloadedData });

  return (
    <Card
    body
    border='primary'
    className={`row ${(isSubmitSuccessful || isSubmitting) ? "disabled-element" : ""}`}
  >

    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Row>
        <Form.Group as={Col} sm='12'>
          <Form.Label>Name</Form.Label>
          <Form.Control
            type='text'
            placeholder='Name of cron workflow'
            defaultValue=''
            {...register("name", {
              required: { value: true, message: "The name is required" },
              maxLength: { value: 50, message: "The max length is 50" },
            })}
            isInvalid={!!formErrors.name}
          />
          <Form.Control.Feedback type='invalid' tooltip>
            {formErrors?.name?.message}
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md='12'>
          <Form.Label>Description</Form.Label>
          <Form.Control
            as='textarea'
            rows={5}
            placeholder='Description of workflow'
            defaultValue=''
            {...register("description", {
              required: { value: true, message: "The description is required" },
              maxLength: { value: 500, message: "The max length is 500" },
            })}
            isInvalid={!!formErrors.description}
          />
          <Form.Control.Feedback type='invalid' tooltip>
            {formErrors?.description?.message}
          </Form.Control.Feedback>
        </Form.Group>
      </Form.Row>
      <Button variant='outline-warning' className='mx-1' onClick={onCancel}>
        Cancel
      </Button>
      <Button type='submit' className='mx-1'>
        {preloadedData ? "Update" : "Create"} workflow
      </Button>
    </Form>
  </Card>
  );
};

WorkflowForm.propTypes = {
  preloadedData: PropTypes.object,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
};

export default WorkflowForm;
