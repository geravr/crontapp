import PropTypes from "prop-types";

import { useEffect } from "react";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Hook Form
import { useForm } from "react-hook-form";
import { useWatch } from "react-hook-form";

// Date data
import { monthsOfYear } from "@data/date";

// Helpers
import {
  getTranslatedValuesToCronFormat,
  getTranslatedValuesFromCronFormat,
} from "@utils/translators";

const MonthForm = (props) => {
  const { isVisible, hookForm, preloadedData } = props;

  const initialData = {
    periodicityType: "everyMonth",
    choosedValues: ["JAN"],
    ...(preloadedData &&
      getTranslatedValuesFromCronFormat(preloadedData, "Month")),
  };

  // Global form
  const { setValue } = hookForm;

  // Local form
  const {
    control,
    register,
    formState: { errors },
  } = useForm({
    defaultValues: initialData,
  });

  const monthWatch = useWatch({ control });

  const { periodicityType, choosedValues } = monthWatch;

  /*************** Lifecycle ***************/
  useEffect(() => {
    // Translate to cron format
    const month = getTranslatedValuesToCronFormat(monthWatch);
    // Set to global form
    setValue("scheduling[4]", month);
  }, [monthWatch]);

  return (
    <div className={isVisible ? "" : "d-none"}>
      <Form.Row>
        <Form.Group as={Col}>
          <Form.Label>Select a periodicity</Form.Label>
          <Form.Control
            as='select'
            defaultValue={periodicityType}
            {...register("periodicityType")}
          >
            <option value={"everyMonth"}>Every month</option>
            <option value={"intervalValues"}>
              Every "x" month(s) starting at month "x"
            </option>
            <option value={"choosedValues"}>
              Specific month (choose one or many)
            </option>
            <option value={"betweenValues"}>
              Every month between month "x" and month "x"
            </option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
      <Form.Row>
        {/* Interval months of year */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "intervalValues" ? "" : "d-none"
          }`}
        >
          Every
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='m-1 w-auto'
            {...register("intervalValues.eachValue")}
          >
            {monthsOfYear.map((month) => {
              month = month.number;
              return (
                <option value={month} key={month}>
                  {month}
                </option>
              );
            })}
          </Form.Control>
          month(s) starting at month
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='m-1'
            {...register("intervalValues.startValue")}
          >
            {monthsOfYear.map((month) => (
              <option value={month.number} key={month.number}>
                {month.name}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        {/* Choose months of year */}
        <Form.Group
          as={Col}
          className={`${periodicityType == "choosedValues" ? "" : "d-none"}`}
        >
          <Row>
            {monthsOfYear.map((month) => (
              <Col xs={6} md={3} lg={2} key={month.number}>
                <Form.Check
                  custom
                  type='checkbox'
                  id={`check-month-${month.name}`}
                  label={month.name}
                  value={month.code}
                  defaultChecked={choosedValues?.includes(month.code)}
                  {...register("choosedValues", {
                    required: {
                      value: true,
                      message: "You must select at least one option",
                    },
                  })}
                />
              </Col>
            ))}
          </Row>
          <p className='mb-0 mt-2 text-danger'>
            {errors?.month?.choosedValues?.message}
          </p>
        </Form.Group>

        {/* Between months of year */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "betweenValues" ? "" : "d-none"
          }`}
        >
          Every month between month
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='m-1'
            {...register("betweenValues.valueOne")}
          >
            {monthsOfYear.map((month) => (
              <option value={month.number} key={month.number}>
                {month.name}
              </option>
            ))}
          </Form.Control>
          and month
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='m-1'
            {...register("betweenValues.valueTwo")}
          >
            {monthsOfYear.map((month) => (
              <option value={month.number} key={month.number}>
                {month.name}
              </option>
            ))}
          </Form.Control>
        </Form.Group>
      </Form.Row>
    </div>
  );
};

MonthForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
  preloadedData: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

export default MonthForm;
