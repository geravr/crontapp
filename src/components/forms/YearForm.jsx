import { useEffect } from "react";
import PropTypes from "prop-types";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Hook Form
import { useForm } from "react-hook-form";
import { useWatch } from "react-hook-form";

// Helpers
import { getNumbersInArray } from "@arrayHelpers";
import { getCurrentDate, getNextYearsInArray } from "@dateHelpers";
import {
  getTranslatedValuesToCronFormat,
  getTranslatedValuesFromCronFormat,
} from "@utils/translators";

const YearForm = (props) => {
  /************** States ***************/
  const { isVisible, hookForm, preloadedData } = props;

  const currentYear = getCurrentDate("yyyy");
  const years = getNumbersInArray(100);
  const nextHundredYears = getNextYearsInArray(100);

  const initialData = {
    periodicityType: "everyYear",
    choosedValues: currentYear,
    ...(preloadedData &&
      getTranslatedValuesFromCronFormat(preloadedData, "Year")),
  };

  // Global form
  const { setValue } = hookForm;

  // Local form
  const {
    control,
    register,
    formState: { errors },
  } = useForm({
    defaultValues: initialData,
  });

  const yearWatch = useWatch({ control });
  const { periodicityType, choosedValues } = yearWatch;

  /*************** Lifecycle ***************/
  useEffect(() => {
    // Translate to cron format
    const year = getTranslatedValuesToCronFormat(yearWatch);
    // Set to global form
    setValue("scheduling[6]", year);
  }, [yearWatch]);

  return (
    <div className={isVisible ? "" : "d-none"}>
      <Form.Row>
        <Form.Group as={Col}>
          <Form.Label>Select a periodicity</Form.Label>
          <Form.Control
            as='select'
            defaultValue={periodicityType}
            {...register("periodicityType")}
          >
            <option value={"everyYear"}>Every year</option>
            <option value={"intervalValues"}>
              Every "x" year(s) starting at year "x"
            </option>
            <option value={"choosedValues"}>
              Specific year (choose one or many)
            </option>
            <option value={"betweenValues"}>
              Every year between year "x" and year "x"
            </option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
      <Form.Row>
        {/* Interval years */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "intervalValues" ? "" : "d-none"
          }`}
        >
          Every
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='mx-1'
            {...register("intervalValues.eachValue")}
          >
            {years.map((year) => {
              year = parseInt(year) + 1;
              return (
                <option value={year} key={year}>
                  {year}
                </option>
              );
            })}
          </Form.Control>
          year(s) starting at year
          <Form.Control
            as='select'
            defaultValue={currentYear}
            size='sm'
            className='m-1'
            {...register("intervalValues.startValue")}
          >
            {nextHundredYears.map((year) => (
              <option value={year} key={year}>
                {year}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        {/* Choose years */}
        <Form.Group
          as={Col}
          className={`${periodicityType == "choosedValues" ? "" : "d-none"}`}
        >
          <Row>
            {nextHundredYears.map((year) => (
              <Col xs={3} sm={2} lg={1} key={year}>
                <Form.Check
                  custom
                  type='checkbox'
                  id={`check-year-${year}`}
                  label={year}
                  value={year}
                  defaultChecked={choosedValues?.includes(year)}
                  {...register("choosedValues", {
                    required: {
                      value: true,
                      message: "You must select at least one option",
                    },
                  })}
                />
              </Col>
            ))}
          </Row>
          <p className='mb-0 mt-2 text-danger'>
            {errors?.year?.choosedValues?.message}
          </p>
        </Form.Group>

        {/* Between years */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "betweenValues" ? "" : "d-none"
          }`}
        >
          Every year between year
          <Form.Control
            as='select'
            defaultValue={currentYear}
            size='sm'
            className='mx-1'
            {...register("betweenValues.valueOne")}
          >
            {nextHundredYears.map((year) => (
              <option value={year} key={year}>
                {year}
              </option>
            ))}
          </Form.Control>
          and year
          <Form.Control
            as='select'
            defaultValue={currentYear}
            size='sm'
            className='mx-1'
            {...register("betweenValues.valueTwo")}
          >
            {nextHundredYears.map((year) => (
              <option value={year} key={year}>
                {year}
              </option>
            ))}
          </Form.Control>
        </Form.Group>
      </Form.Row>
    </div>
  );
};

YearForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
  preloadedData: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

export default YearForm;
