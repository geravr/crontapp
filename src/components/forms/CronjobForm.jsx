import { useState } from "react";
import { useRouter } from "next/router";
import PropTypes from "prop-types";

// React hook form
import { useForm } from "react-hook-form";

// Bootstrap
import { Form } from "react-bootstrap";

// steps
import { steps } from "@data/steps";

// Components
import Wizard from "@components/wizard/Wizard";
import InitialForm from "@components/forms/InitialForm";
import SecondsForm from "@components/forms/SecondsForm";
import MinutesForm from "@components/forms/MinutesForm";
import HoursForm from "@components/forms/HoursForm";
import DayForm from "@components/forms/DayForm";
import MonthForm from "@components/forms/MonthForm";
import YearForm from "@components/forms/YearForm";

const CronjobForm = ({ preloadedData, onSubmit, isLoading }) => {
  const [currentStep, setCurrentStep] = useState(1);

  const hasPreloadedData = preloadedData ? true : false;
  const scheduling = preloadedData?.scheduling?.split(" ");
  const router = useRouter();

  /*************** Hook Form ***************/
  const {
    handleSubmit,
    formState: { errors: formErrors, isValid: isValidForm, isSubmitSuccessful, isSubmitting },
    trigger,
    register,
    setValue,
    watch,
  } = useForm({ mode: "onBlur", defaultValues: preloadedData });

  const cronScheduleWatch = watch("scheduling", [ "0", "0", "0", "1/1", "*", "?", "*",])?.join(" ");

  /*************** Functions - Multi step ***************/
  const changeStep = (step) => {
    const hasFormErrors = Object.keys(formErrors).length > 0;
    if (!hasFormErrors && isValidForm) {
      setCurrentStep(step);
      return;
    }
    trigger(["name", "description", "workflow_id"]);
  };

  const handleNext = () => {
    if (currentStep < steps.length) {
      changeStep(currentStep + 1);
    }
  };

  const handlePrevious = () => {
    if (currentStep > 1) {
      setCurrentStep(currentStep - 1);
      return;
    }
    return;
  };

  return (
    <>
      <Wizard
        steps={steps}
        currentStep={currentStep}
        handlePrevious={handlePrevious}
        handleNext={handleNext}
        handleClickStep={changeStep}
        isDisabled={(isSubmitSuccessful || isSubmitting)}
        isUpdate={hasPreloadedData}
      >
        <Wizard.Header
          steps={steps}
          currentStep={currentStep}
          handleClickStep={changeStep}
          subtitle={cronScheduleWatch}
        />
        <Wizard.Body>
          <Form onSubmit={handleSubmit(onSubmit)} id='cronForm'>
            <InitialForm
              isVisible={currentStep == 1}
              hookForm={{ register, formErrors, preloadedData, setValue }}
            />
            <SecondsForm
              isVisible={currentStep == 2}
              hookForm={{ setValue, watch }}
              preloadedData={hasPreloadedData && scheduling[0]}
            />
            <MinutesForm
              isVisible={currentStep == 3}
              hookForm={{ setValue, watch }}
              preloadedData={hasPreloadedData && scheduling[1]}
            />
            <HoursForm
              isVisible={currentStep == 4}
              hookForm={{ setValue, watch }}
              preloadedData={hasPreloadedData && scheduling[2]}
            />
            <DayForm
              isVisible={currentStep == 5}
              hookForm={{ setValue, watch }}
              preloadedData={hasPreloadedData && [scheduling[5], scheduling[3]]}
            />
            <MonthForm
              isVisible={currentStep == 6}
              hookForm={{ setValue, watch }}
              preloadedData={hasPreloadedData && scheduling[4]}
            />
            <YearForm
              isVisible={currentStep == 7}
              hookForm={{ setValue, watch }}
              preloadedData={hasPreloadedData && scheduling[6]}
            />
          </Form>
        </Wizard.Body>
        <Wizard.Footer
          steps={steps}
          currentStep={currentStep}
          handlePrevious={handlePrevious}
          handleNext={handleNext}
          isUpdate={hasPreloadedData}
          onCancel={() => router.push("/")}
        ></Wizard.Footer>
      </Wizard>
    </>
  );
};

CronjobForm.propTypes = {
  preloadedData: PropTypes.object,
  onSubmit: PropTypes.func,
  isLoading: PropTypes.bool,
};

export default CronjobForm;
