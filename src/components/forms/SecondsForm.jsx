import { useEffect } from "react";
import PropTypes from "prop-types";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Hook Form
import { useForm } from "react-hook-form";
import { useWatch } from "react-hook-form";

// Helpers
import { getNumbersInArray } from "@arrayHelpers";
import {
  getTranslatedValuesToCronFormat,
  getTranslatedValuesFromCronFormat,
} from "@utils/translators";

const SecondsForm = (props) => {
  const { isVisible, hookForm, preloadedData } = props;
  const secondsArray = getNumbersInArray(60);

  const initialData = {
    periodicityType: "choosedValues",
    choosedValues: ["0"],
    ...(preloadedData &&
      getTranslatedValuesFromCronFormat(preloadedData, "Seconds")),
  };

  // Global form
  const { setValue } = hookForm;

  // Local form
  const {
    control,
    register,
    formState: { errors },
  } = useForm({ mode: "onBlur", defaultValues: initialData });

  const secondsWatch = useWatch({ control });

  const { periodicityType, choosedValues } = secondsWatch;

  /*************** Lifecycle ***************/
  useEffect(() => {
    // Translate toformat
    const seconds = getTranslatedValuesToCronFormat(secondsWatch);
    // Set to global form
    setValue("scheduling[0]", seconds);
  }, [secondsWatch]);

  return (
    <div className={isVisible ? "" : "d-none"}>
      <Form.Row>
        <Form.Group as={Col}>
          <Form.Label>Select a periodicity</Form.Label>
          <Form.Control
            as='select'
            defaultValue={periodicityType}
            {...register("periodicityType")}
          >
            <option value={"everySeconds"}>Every second</option>
            <option value={"intervalValues"}>
              Every "x" second(s) starting at second "x"
            </option>
            <option value={"choosedValues"}>
              Specific second (choose one or many)
            </option>
            <option value={"betweenValues"}>
              Every second between second "x" and second "x"
            </option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
      <Form.Row>
        {/* Interval seconds */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "intervalValues" ? "" : "d-none"
          }`}
        >
          Every
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='m-1 w-auto'
            {...register("intervalValues.eachValue")}
          >
            {secondsArray.map((second) => {
              second = parseInt(second) + 1;
              return (
                <option value={second} key={second}>
                  {second}
                </option>
              );
            })}
          </Form.Control>
          <div className='mr-1'>second(s)</div> starting at second
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='m-1 w-auto'
            {...register("intervalValues.startValue")}
          >
            {secondsArray.map((second) => (
              <option value={second} key={second}>
                {second}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        {/* Choose seconds */}
        <Form.Group
          as={Col}
          className={`${periodicityType == "choosedValues" ? "" : "d-none"}`}
        >
          <Row>
            {secondsArray.map((second) => (
              <Col xs={3} sm={2} lg={1} key={second}>
                <Form.Check
                  custom
                  type='checkbox'
                  id={`check-second-${second}`}
                  label={second}
                  value={second}
                  defaultChecked={choosedValues?.includes(second)}
                  {...register("choosedValues", {
                    required: "You must select at least one option",
                  })}
                />
              </Col>
            ))}
          </Row>
          <p className='mb-0 mt-2 text-danger'>
            {errors?.choosedValues?.message}
          </p>
        </Form.Group>

        {/* Between seconds of a minute */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "betweenValues" ? "" : "d-none"
          }`}
        >
          Every second between second
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='m-1'
            {...register(`betweenValues.valueOne`)}
          >
            {secondsArray.map((second) => (
              <option value={second} key={second}>
                {second}
              </option>
            ))}
          </Form.Control>
          and second
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='m-1'
            {...register(`betweenValues.valueTwo`)}
          >
            {secondsArray.map((second) => (
              <option value={second} key={second}>
                {second}
              </option>
            ))}
          </Form.Control>
        </Form.Group>
      </Form.Row>
    </div>
  );
};

SecondsForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
  preloadedData: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

export default SecondsForm;
