import { useState, useEffect } from "react";
import PropTypes from "prop-types";

// Bootstrap
import { Form, Col } from "react-bootstrap";

import { getWorkflows } from "@api";

const InitialForm = ({ isVisible, hookForm }) => {
  const { register, formErrors, preloadedData, setValue } = hookForm;
  const [workflows, setWorkflows] = useState([]);

  /*************** Lifecycle ***************/
  useEffect(() => {
    const fetchWorkflows = async () => {
      const { data: workflows } = await getWorkflows();
      setWorkflows(workflows);
      setValue("workflow_id", preloadedData?.workflow_id);
    };
    fetchWorkflows();
  }, []);

  return (
    <Form.Row className={isVisible ? "" : "d-none"}>
      <Col md='4'>
        <Form.Row>
          <Form.Group as={Col} sm='12'>
            <Form.Label>Name</Form.Label>
            <Form.Control
              type='text'
              placeholder='Name of cron job'
              defaultValue=''
              {...register("name", {
                required: { value: true, message: "The name is required" },
                maxLength: { value: 50, message: "The max length is 50" },
              })}
              isInvalid={!!formErrors.name}
            />
            <Form.Control.Feedback type='invalid' tooltip>
              {formErrors?.name?.message}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} sm='12'>
            <Form.Label>Workflow</Form.Label>
            <Form.Control
              as='select'
              {...register("workflow_id", {
                required: { value: true, message: "The workflow is required" },
              })}
              isInvalid={!!formErrors.workflow_id}
              defaultValue={""}
            >
              <option hidden disabled label='Select an option' value='' />
              {workflows.map((workflow) => (
                <option value={workflow.id} key={workflow.id}>
                  {workflow.name}
                </option>
              ))}
            </Form.Control>
            <Form.Control.Feedback type='invalid' tooltip>
              {formErrors?.workflow_id?.message}
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
      </Col>
      <Form.Group as={Col} md='8'>
        <Form.Label>Description</Form.Label>
        <Form.Control
          as='textarea'
          rows={5}
          placeholder='Description of cron job'
          defaultValue=''
          {...register("description", {
            required: { value: true, message: "The description is required" },
            maxLength: { value: 500, message: "The max length is 500" },
          })}
          isInvalid={!!formErrors.description}
        />
        <Form.Control.Feedback type='invalid' tooltip>
          {formErrors?.description?.message}
        </Form.Control.Feedback>
      </Form.Group>
    </Form.Row>
  );
};

InitialForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
};

export default InitialForm;
