import { useEffect } from "react";
import PropTypes from "prop-types";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Hook Form
import { useForm } from "react-hook-form";
import { useWatch } from "react-hook-form";

// Helpers
import {
  getTranslatedValuesToCronFormat,
  getTranslatedValuesFromCronFormat,
} from "@utils/translators";

// components
import WeeklyDayForm from "@components/forms/WeeklyDayForm";
import MonthlyDayForm from "@components/forms/MonthlyDayForm";

const DayForm = (props) => {
  const { isVisible, hookForm, preloadedData } = props;
  const initialData = {
    recurrence: "monthly",
    ...(preloadedData && {
      recurrence: preloadedData[0] === "?" ? "monthly" : "weekly",
    }),
    weekly: {
      choosedValues: "SUN",
      periodicityType: "everyDay",
      ...(preloadedData &&
        getTranslatedValuesFromCronFormat(preloadedData[0], "Day", "everyDay")),
    },
    monthly: {
      periodicityType: "intervalValues",
      intervalValues: { eachValue: "1", startValue: "1" },
      choosedValues: "1",
      ...(preloadedData &&
        getTranslatedValuesFromCronFormat(
          preloadedData[1],
          "Day",
          "intervalValues"
        )),
    },
  };

  // Global form
  const { setValue } = hookForm;

  // Local form
  const {
    control,
    register,
    formState: { errors },
  } = useForm({ defaultValues: initialData });

  const dayWatch = useWatch({ control });

  const { recurrence, weekly, monthly } = dayWatch;

  const isWeekly = recurrence === "weekly";
  const isMonthly = recurrence === "monthly";

  /*************** Lifecycle ***************/
  useEffect(() => {
    if (isWeekly) {
      // Translate to cron format
      const weekly = getTranslatedValuesToCronFormat(dayWatch.weekly);

      // Set to global form
      setValue("scheduling[5]", weekly); // weekly
      setValue("scheduling[3]", "?"); // monthly
    }

    if (isMonthly) {
      // Translate to cron format
      const monthly = getTranslatedValuesToCronFormat(dayWatch.monthly);

      // Set to global form
      setValue("scheduling[5]", "?"); // weekly
      setValue("scheduling[3]", monthly); // monthly
    }
  }, [dayWatch]);

  return (
    <div className={isVisible ? "" : "d-none"}>
      <Form.Row>
        <Form.Group as={Col} md='6' lg='4'>
          <Form.Label>Select a recurrence</Form.Label>
          <Row>
            <Col xs={6} md={3}>
              <Form.Check
                custom
                type='radio'
                id={`radio-weekly`}
                label='Weekly'
                value='weekly'
                defaultChecked={recurrence === "weekly"}
                {...register("recurrence")}
              />
            </Col>
            <Col xs={6} md={3}>
              <Form.Check
                custom
                type='radio'
                id={`radio-monthly`}
                label='Montly'
                value='monthly'
                defaultChecked={recurrence === "monthly"}
                {...register("recurrence")}
              />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group as={Col}>
          <Form.Label>Select a periodicity</Form.Label>
          <Form.Control
            as='select'
            defaultValue={weekly.periodicityType}
            className={recurrence === "weekly" ? "" : "d-none"}
            {...register("weekly.periodicityType")}
          >
            <option value={"everyDay"}>Every day</option>
            <option value={"intervalValues"}>
              Every "x" day(s) starting at day "x"
            </option>
            <option value={"choosedValues"}>
              Specific day of week (choose one or many)
            </option>
            <option value={"selectedValue"}>
              On the last "day_of_week" of the month
            </option>
            <option value={"intervalValuesFormatTwo"}>
              On the "ordinal_number_day" "day_of_week" of the month
            </option>
          </Form.Control>

          <Form.Control
            as='select'
            defaultValue={monthly.periodicityType}
            className={recurrence === "monthly" ? "" : "d-none"}
            {...register("monthly.periodicityType")}
          >
            <option value={"intervalValues"}>
              Every "x" day(s) starting on the "x"
            </option>

            <option value={"choosedValues"}>
              Specific day of month (choose one or many)
            </option>
            <option value={"lastDayOfMonth"}>
              On the last day of the month
            </option>
            <option value={"lastWeekDayOfMonth"}>
              On the last weekday of the month
            </option>

            <option value={"selectedDayBeforeEndOfMonth"}>
              "day_of_month" day(s) before the end of month
            </option>
            <option value={"selectedNearestWeekDay"}>
              Nearest weekday (Monday to Friday) to the "day_of_month" of the
              month
            </option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
      <WeeklyDayForm
        isVisible={isWeekly}
        hookForm={{
          register,
          errors,
          periodicityType: weekly.periodicityType,
          weekly,
        }}
      />
      <MonthlyDayForm
        isVisible={isMonthly}
        hookForm={{
          register,
          errors,
          periodicityType: monthly.periodicityType,
          monthly,
        }}
      />
    </div>
  );
};

DayForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
  preloadedData: PropTypes.oneOfType([PropTypes.bool, PropTypes.array]),
};

export default DayForm;
