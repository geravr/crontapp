import PropTypes from "prop-types";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Date data
import { monthDays } from "@data/date";

const MonthlyDayForm = ({ isVisible, hookForm }) => {
  const { register, errors, monthly } = hookForm;

  return (
    <Form.Row className={isVisible ? "" : "d-none"}>
      {/* Interval days of month */}
      <Form.Group
        as={Col}
        className={`form-inline ${
          monthly.periodicityType == "intervalValues" ? "" : "d-none"
        }`}
      >
        Every
        <Form.Control
          as='select'
          defaultValue='1'
          size='sm'
          className='m-1 w-auto'
          {...register("monthly.intervalValues.eachValue")}
        >
          {monthDays.map((day) => {
            day = day.number;
            return (
              <option value={day} key={day}>
                {day}
              </option>
            );
          })}
        </Form.Control>
        day(s) starting on the
        <Form.Control
          as='select'
          defaultValue='1'
          size='sm'
          className='m-1 w-auto'
          {...register("monthly.intervalValues.startValue")}
        >
          {monthDays.map((day) => (
            <option value={day.number} key={day.number}>
              {day.ordinalNumber}
            </option>
          ))}
        </Form.Control>{" "}
        of the month
      </Form.Group>

      {/* Choose days of month */}
      <Form.Group
        as={Col}
        className={`${
          monthly.periodicityType == "choosedValues" ? "" : "d-none"
        }`}
      >
        <Row>
          {monthDays.map((day) => (
            <Col xs={6} md={3} key={day.number}>
              <Form.Check
                custom
                type='checkbox'
                id={`check-dm-${day.number}`}
                label={day.number}
                value={day.number}
                defaultChecked={monthly.choosedValues?.includes(day.number)}
                {...register("monthly.choosedValues", {
                  required: {
                    value: true,
                    message: "You must select at least one option",
                  },
                })}
              />
            </Col>
          ))}
        </Row>
        <p className='mb-0 mt-2 text-danger'>
          {errors?.monthly?.choosedValues?.message}
        </p>
      </Form.Group>

      {/* Select day before end of the month */}
      <Form.Group
        as={Col}
        className={`form-inline ${
          monthly.periodicityType == "selectedDayBeforeEndOfMonth"
            ? ""
            : "d-none"
        }`}
      >
        <Form.Control
          as='select'
          defaultValue='L-1'
          size='sm'
          className='mx-1 w-auto'
          {...register("monthly.selectedDayBeforeEndOfMonth.selectedValue")}
        >
          {monthDays.map((day) => {
            return (
              <option value={day.lastDayCode} key={day.lastDayCode}>
                {day.number}
              </option>
            );
          })}
        </Form.Control>
        <div>day(s)</div> before the end of the month
      </Form.Group>

      {/* Select nearest weekday */}
      <Form.Group
        as={Col}
        className={`form-inline ${
          monthly.periodicityType == "selectedNearestWeekDay" ? "" : "d-none"
        }`}
      >
        Nearest weekday (Monday to Friday) to the
        <Form.Control
          as='select'
          defaultValue='1W'
          size='sm'
          className='mx-1 w-auto'
          {...register("monthly.selectedNearestWeekDay.selectedValue")}
        >
          {monthDays.map((day) => {
            return (
              <option value={day.weekDayCode} key={day.weekDayCode}>
                {day.ordinalNumber}
              </option>
            );
          })}
        </Form.Control>
        of the month
      </Form.Group>
    </Form.Row>
  );
};

MonthlyDayForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
};

export default MonthlyDayForm;
