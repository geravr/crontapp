import PropTypes from "prop-types";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Date data
import { weekDays } from "@data/date";

const WeeklyDayForm = ({ isVisible, hookForm }) => {
  const { register, errors, weekly } = hookForm;
  const daysWeekMonToFri = weekDays.slice(0, 5);

  return (
    <Form.Row className={isVisible ? "" : "d-none"}>
      {/* Interval days of week */}
      <Form.Group
        as={Col}
        className={`form-inline ${
          weekly.periodicityType == "intervalValues" ? "" : "d-none"
        }`}
      >
        Every
        <Form.Control
          as='select'
          defaultValue='1'
          size='sm'
          className='m-1 w-auto'
          {...register("weekly.intervalValues.eachValue")}
        >
          {weekDays.map((day) => {
            day = day.number;
            return (
              <option value={day} key={day}>
                {day}
              </option>
            );
          })}
        </Form.Control>
        day(s) starting at day
        <Form.Control
          as='select'
          defaultValue='1'
          size='sm'
          className='m-1'
          {...register("weekly.intervalValues.startValue")}
        >
          {weekDays.map((day) => (
            <option value={day.number} key={day.number}>
              {day.name}
            </option>
          ))}
        </Form.Control>
      </Form.Group>

      {/* Choose days of week */}
      <Form.Group
        as={Col}
        className={`${
          weekly.periodicityType == "choosedValues" ? "" : "d-none"
        }`}
      >
        <Row>
          {weekDays.map((day) => (
            <Col xs={6} md={3} key={day.number}>
              <Form.Check
                custom
                type='checkbox'
                id={`check-dw-${day.name}`}
                label={day.name}
                value={day.code}
                defaultChecked={weekly.choosedValues?.includes(day.code)}
                {...register("weekly.choosedValues", {
                  required: {
                    value: true,
                    message: "You must select at least one option",
                  },
                })}
              />
            </Col>
          ))}
        </Row>
        <p className='mb-0 mt-2 text-danger'>
          {errors?.day?.weekly?.choosedValues?.message}
        </p>
      </Form.Group>

      {/* Select last day_week of month */}
      <Form.Group
        as={Col}
        className={`form-inline ${
          weekly.periodicityType == "selectedValue" ? "" : "d-none"
        }`}
      >
        On the last
        <Form.Control
          as='select'
          defaultValue='1L'
          size='sm'
          className='mx-1 w-auto'
          {...register("weekly.selectedValue")}
        >
          {weekDays.map((day) => {
            return (
              <option value={day.lastDayCode} key={day.lastDayCode}>
                {day.name}
              </option>
            );
          })}
        </Form.Control>
        of the month
      </Form.Group>

      {/* Between days of week */}
      <Form.Group
        as={Col}
        className={`form-inline ${
          weekly.periodicityType == "intervalValuesFormatTwo" ? "" : "d-none"
        }`}
      >
        On the
        <Form.Control
          as='select'
          defaultValue='1'
          size='sm'
          className='mx-1 w-auto'
          {...register("weekly.intervalValues.eachValue")}
        >
          {daysWeekMonToFri.map((day) => (
            <option value={day.number} key={day.number}>
              {day.ordinalNumber}
            </option>
          ))}
        </Form.Control>
        <Form.Control
          as='select'
          defaultValue='1'
          size='sm'
          className='mx-1 w-auto'
          {...register("weekly.intervalValues.startValue")}
        >
          {weekDays.map((day) => (
            <option value={day.number} key={day.number}>
              {day.name}
            </option>
          ))}
        </Form.Control>
        of the month
      </Form.Group>
    </Form.Row>
  );
};

WeeklyDayForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
};

export default WeeklyDayForm;
