import PropTypes from "prop-types";

import { useEffect } from "react";

// Bootstrap
import { Form, Row, Col } from "react-bootstrap";

// Hook Form
import { useForm } from "react-hook-form";
import { useWatch } from "react-hook-form";

// Helpers
import { getNumbersInArray } from "@arrayHelpers";
import {
  getTranslatedValuesToCronFormat,
  getTranslatedValuesFromCronFormat,
} from "@utils/translators";

const MinutesForm = (props) => {
  const { isVisible, hookForm, preloadedData } = props;
  const minutes = getNumbersInArray(60);

  const initialData = {
    periodicityType: "choosedValues",
    choosedValues: ["0"],
    ...(preloadedData &&
      getTranslatedValuesFromCronFormat(preloadedData, "Minutes")),
  };

  // Global form
  const { setValue } = hookForm;

  // Local form
  const {
    control,
    register,
    formState: { errors },
  } = useForm({
    defaultValues: initialData,
  });

  const minutesWatch = useWatch({ control });

  const { periodicityType, choosedValues } = minutesWatch;

  /*************** Lifecycle ***************/
  useEffect(() => {
    // Translate to cron format
    const minutes = getTranslatedValuesToCronFormat(minutesWatch);
    // Set to global form
    setValue("scheduling[1]", minutes);
  }, [minutesWatch]);

  return (
    <div className={isVisible ? "" : "d-none"}>
      <Form.Row>
        <Form.Group as={Col}>
          <Form.Label>Select a periodicity</Form.Label>
          <Form.Control
            as='select'
            defaultValue={periodicityType}
            {...register("periodicityType")}
          >
            <option value={"everyMinutes"}>Every minute</option>
            <option value={"intervalValues"}>
              Every "x" minute(s) starting at minute "x"
            </option>
            <option value={"choosedValues"}>
              Specific minute (choose one or many)
            </option>
            <option value={"betweenValues"}>
              Every minute between minute "x" and minute "x"
            </option>
          </Form.Control>
        </Form.Group>
      </Form.Row>
      <Form.Row>
        {/* Interval minutes */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "intervalValues" ? "" : "d-none"
          }`}
        >
          Every
          <Form.Control
            as='select'
            defaultValue='1'
            size='sm'
            className='m-1 w-auto'
            {...register("intervalValues.eachValue")}
          >
            {minutes.map((minute) => {
              minute = parseInt(minute) + 1;
              return (
                <option value={minute} key={minute}>
                  {minute}
                </option>
              );
            })}
          </Form.Control>
          <div className='mr-1'>minute(s)</div> starting at minute
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='m-1 w-auto'
            {...register("intervalValues.startValue")}
          >
            {minutes.map((minute) => (
              <option value={minute} key={minute}>
                {minute}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        {/* Choose minutes */}
        <Form.Group
          as={Col}
          className={`${periodicityType == "choosedValues" ? "" : "d-none"}`}
        >
          <Row>
            {minutes.map((minute) => (
              <Col xs={3} sm={2} lg={1} key={minute}>
                <Form.Check
                  custom
                  type='checkbox'
                  id={`check-minute-${minute}`}
                  label={minute}
                  value={minute}
                  defaultChecked={choosedValues?.includes(minute)}
                  {...register("choosedValues", {
                    required: "You must select at least one option",
                  })}
                />
              </Col>
            ))}
          </Row>
          <p className='mb-0 mt-2 text-danger'>
            {errors?.minutes?.choosedValues?.message}
          </p>
        </Form.Group>

        {/* Between minutes of an hour */}
        <Form.Group
          as={Col}
          className={`form-inline ${
            periodicityType == "betweenValues" ? "" : "d-none"
          }`}
        >
          Every minute between minute
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='m-1'
            {...register(`betweenValues.valueOne`)}
          >
            {minutes.map((minute) => (
              <option value={minute} key={minute}>
                {minute}
              </option>
            ))}
          </Form.Control>
          and minute
          <Form.Control
            as='select'
            defaultValue='0'
            size='sm'
            className='m-1'
            {...register(`betweenValues.valueTwo`)}
          >
            {minutes.map((minute) => (
              <option value={minute} key={minute}>
                {minute}
              </option>
            ))}
          </Form.Control>
        </Form.Group>
      </Form.Row>
    </div>
  );
};

MinutesForm.propTypes = {
  isVisible: PropTypes.bool,
  hookForm: PropTypes.object,
  preloadedData: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

export default MinutesForm;
