import PropTypes from "prop-types";

// Bootstrap
import { Container } from 'react-bootstrap';

const Main = ({ children }) => {
  return (
    <main className="px-2">
      <Container className="my-4">{children}</Container>
    </main>
  );
};

Main.propTypes = {
  children: PropTypes.node,
};

export default Main;
