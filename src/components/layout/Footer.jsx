const Footer = () => {
  return (
    <footer className='mt-auto py-3 text-light text-center bg-dark'>
      <a
        href='https://www.backbonesystems.io/'
        target='_blank'
        rel='noopener noreferrer'
        className="text-secondary-light"
      >
        Backbone challenge
      </a>
    </footer>
  );
};

export default Footer;
