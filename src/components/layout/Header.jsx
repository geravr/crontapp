import Link from "next/link";
import ActiveLink from "./ActiveLink";

// Bootstrap
import { Navbar, Nav, Container } from "react-bootstrap";

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock, faCalendar } from "@fortawesome/free-solid-svg-icons";

const Header = () => {
  return (
    <header>
      <Navbar bg='dark' variant='dark'>
        <Container className='justify-content-between flex-column flex-md-row'>
          <Link href='/' passHref>
            <Navbar.Brand className='py-0 d-flex'>
              <span className='fa-layers fa-fw fa-2x mr-1'>
                <FontAwesomeIcon icon={faCalendar} />
                <FontAwesomeIcon
                  icon={faClock}
                  transform='shrink-6 right-4 down-5'
                  className='text-primary'
                />
              </span>
              <h2>Crontapp</h2>
            </Navbar.Brand>
          </Link>
          <Nav>
            <ActiveLink href='/' activeClassName='active' passHref>
              <Nav.Link>Cronjob</Nav.Link>
            </ActiveLink>
            <ActiveLink href='/workflow' activeClassName='active' passHref>
              <Nav.Link href='#features'>Workflow</Nav.Link>
            </ActiveLink>
          </Nav>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
