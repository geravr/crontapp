import Link from "next/link";
import PropTypes from "prop-types";

// Bootstrap
import { Button } from "react-bootstrap";

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrashAlt } from "@fortawesome/free-solid-svg-icons";

// Cronstrue
import cronstrue from "cronstrue";

const CronjobItemList = ({ cronjob, openDeleteModal }) => {
  /************** Functions ***************/
  const getExcerpt = (content) => {
    if (content.length <= 50) {
      return content;
    }
    return content.substring(0, 50) + "...";
  };

  const excerpt = getExcerpt(cronjob.description);
  const periodicity = cronstrue.toString(cronjob.scheduling);

  return (
    <tr>
      <td style={{ minWidth: "15vw" }}>
        <Link href={`/cronjob/${cronjob.id}`} passHref>
          <a>{cronjob.name}</a>
        </Link>
      </td>
      <td style={{ minWidth: "20vw" }}>{excerpt}</td>
      <td>{periodicity}</td>
      <td>
        <div className='d-flex justify-content-center'>
          <Link href={`/cronjob/edit/${cronjob.id}`} passHref>
            <a className='btn btn-outline-secondary mx-1' data-testid='edit-button'>
              <FontAwesomeIcon icon={faPen} />
            </a>
          </Link>
          <Button
            data-testid='delete-button'
            variant='outline-danger'
            onClick={() => openDeleteModal(cronjob.id)}
          >
            <FontAwesomeIcon icon={faTrashAlt} />
          </Button>
        </div>
      </td>
    </tr>
  );
};

CronjobItemList.propTypes = {
  cronjob: PropTypes.object.isRequired,
  openDeleteModal: PropTypes.func,
};

export default CronjobItemList;
