import Head from "next/head";
import PropTypes from 'prop-types';

const Seo = (props) => {
  const { title, description } = props;
  return (
    <Head>
      <meta name='description' content={description} />
      <title>{title}</title>
    </Head>
  );
};

Seo.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
};

export default Seo;
