import PropTypes from "prop-types";

const ListBody = ({ children }) => {
  return (
    <tbody>
      {children}
    </tbody>
  );
};

ListBody.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ListBody;
