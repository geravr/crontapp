import PropTypes from "prop-types";

// Bootstrap
import { Row, Card, Table } from "react-bootstrap";

// Components
import ListHeader from "./ListHeader";
import ListBody from "./ListBody";

const List = ({ children }) => {
  return (
    <Row className='justify-content-center justify-content-md-start px-1'>
      <Card className='col overflow-auto px-0'>
        <Table borderless striped style={{ minWidth: "35rem" }} className=''>
          {children}
        </Table>
      </Card>
    </Row>
  );
};

List.propTypes = {
  children: PropTypes.node.isRequired,
};

List.Header = ListHeader;
List.Body = ListBody;

export default List;
