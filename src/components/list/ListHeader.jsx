import PropTypes from "prop-types";

const ListHeader = ({ children, className = "bg-primary text-light" }) => {
  return (
    <thead className={className}>
      <tr>{children}</tr>
    </thead>
  );
};

ListHeader.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

export default ListHeader;
