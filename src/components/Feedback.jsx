import { useEffect } from "react";
import PropTypes from 'prop-types';

// Bootstrap
import { Alert } from "react-bootstrap";

// Styles
import style from "@styleModules/Feedback.module.scss";

const Feedback = (props) => {
  /************** Destructuring ***************/
  const {
    type = "success",
    isVisible,
    title,
    message,
    onClose,
    intervalClose = 5000,
  } = props;

  /************** Lifecycle ***************/
  useEffect(() => {
    if (isVisible) {
      setTimeout(() => {
        onClose();
      }, intervalClose);
    }
  }, [isVisible]);

  return (
    <Alert
      show={isVisible}
      variant={type}
      onClose={onClose}
      dismissible
      className={style.alert}
    >
      {title && <Alert.Heading>{title}</Alert.Heading>}
      {message && <p className="py-0 my-0">{message}</p>}
    </Alert>
  );
};

Feedback.propTypes = {
  type: PropTypes.string,
  isVisible: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func,
  intervalClose: PropTypes.number,
};

export default Feedback;
