/************** Translators to cron format **************/
const translateIntervalValuesToCronFormat = (values, option = 1) => {
  const { eachValue, startValue } = values?.intervalValues;
  if (option === 1) {
    return `${startValue}/${eachValue}`;
  }
  if (option === 2) {
    return `${startValue}#${eachValue}`;
  }
};

const translateChoosedValuesToCronFormat = (values) => {
  const { choosedValues } = values;
  if (choosedValues.length === 0) {
    return "0";
  }
  return choosedValues?.toString();
};

const translateBetweenValuesToCronFormat = (values) => {
  const { valueOne, valueTwo } = values?.betweenValues;
  return `${valueOne}-${valueTwo}`;
};

const translateSelectedValueToCronFormat = (values) => {
  const { selectedValue } = values;
  return selectedValue?.toString();
};

/************** Translators to cron format - Global ***************/
export const getTranslatedValuesToCronFormat = (values) => {
  const periodicityType = values?.periodicityType;

  // Generics
  if (periodicityType === "intervalValues") {
    return translateIntervalValuesToCronFormat(values);
  }
  if (periodicityType === "intervalValuesFormatTwo") {
    return translateIntervalValuesToCronFormat(values, 2);
  }
  if (periodicityType === "choosedValues") {
    return translateChoosedValuesToCronFormat(values);
  }
  if (periodicityType === "betweenValues") {
    return translateBetweenValuesToCronFormat(values);
  }
  if (periodicityType === "selectedValue") {
    return translateSelectedValueToCronFormat(values);
  }

  // MonthlyDayForms
  if (periodicityType === "lastDayOfMonth") {
    return "L";
  }
  if (periodicityType === "lastWeekDayOfMonth") {
    return "LW";
  }
  if (periodicityType === "selectedDayBeforeEndOfMonth") {
    return translateSelectedValueToCronFormat(
      values.selectedDayBeforeEndOfMonth
    );
  }
  if (periodicityType === "selectedNearestWeekDay") {
    return translateSelectedValueToCronFormat(values.selectedNearestWeekDay);
  }
  return "*";
};

/************** Translators from cron format **************/
const translateIntervalValuesFromCronFormat = (values, option = 1) => {
  const isOption1 = option === 1;
  const intervalValues = values.split(isOption1 ? "/" : "#");
  return {
    periodicityType: isOption1 ? "intervalValues" : "intervalValuesFormatTwo",
    intervalValues: {
      eachValue: intervalValues[1],
      startValue: intervalValues[0],
    },
  };
};

const translateChoosedValuesFromCronFormat = (values) => {
  const choosedValues = values.split(",");
  return {
    periodicityType: "choosedValues",
    choosedValues: [...choosedValues],
  };
};

const translateBetweenValuesFromCronFormat = (values) => {
  const betweenValues = values.split("-");
  return {
    periodicityType: "betweenValues",
    betweenValues: {
      valueOne: betweenValues[0],
      valueTwo: betweenValues[1],
    },
  };
};

const translateDayBeforeEndOfMonthFromCronFormat = (values) => {
  return {
    periodicityType: "selectedDayBeforeEndOfMonth",
    selectedDayBeforeEndOfMonth: {
      selectedValue: values,
    },
  };
};

const translateNearestWeekDayFromCronFormat = (values) => {
  return {
    periodicityType: "selectedNearestWeekDay",
    selectedNearestWeekDay: {
      selectedValue: values,
    },
  };
};

/************** Translators from cron format - Global ***************/
export const getTranslatedValuesFromCronFormat = (values, ctx, dafultPeriodicity) => {
  // Regex
  const WeekDaysCode = /^((?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:, )?)(?:1)*$/gm;
  const onlyNumber = /^[0-9]*$/gm;

  const isInterval = values?.includes("/");
  const isIntervalFormatTwo = values?.includes("#");
  const isChoosed = (values?.includes(",") || values.match(WeekDaysCode) || values.match(onlyNumber));
  const isBetween = values?.includes("-");
  const isEvery = values?.includes("*");
  const isLastDayOfMonth = values === "L";
  const isLastWeekDayOfMonth = values === "LW";
  const isNearestWeekDay = values?.includes("W");
  const isDayBeforeEndOfMonth = values?.includes("L-");

  if (values === "?") {
    return {
      periodicityType: dafultPeriodicity,
    };
  }
  if (isInterval) {
    return translateIntervalValuesFromCronFormat(values);
  }
  if (isIntervalFormatTwo) {
    return translateIntervalValuesFromCronFormat(values, 2);
  }
  if (isChoosed) {
    return translateChoosedValuesFromCronFormat(values);
  }
  if (isDayBeforeEndOfMonth) {
    return translateDayBeforeEndOfMonthFromCronFormat(values);
  }
  if (isBetween) {
    return translateBetweenValuesFromCronFormat(values);
  }
  if (isEvery) {
    return {
      periodicityType: `every${ctx}`,
    };
  }
  if (isLastDayOfMonth) {
    return {
      periodicityType: "lastDayOfMonth",
    };
  }
  if (isLastWeekDayOfMonth) {
    return {
      periodicityType: "lastWeekDayOfMonth",
    };
  }
  if (isNearestWeekDay) {
    return translateNearestWeekDayFromCronFormat(values);
  }
  return {
    periodicityType: "selectedValue",
    selectedValue: values,
  };
};
