import axios from "axios";

/*************** Default axios config ***************/
axios.defaults.headers.common[
  "Authorization"
] = `Bearer ${process.env.NEXT_PUBLIC_TOKEN_API}`;
axios.defaults.baseURL = process.env.NEXT_PUBLIC_HOST_API;

/*************** Cronjobs ***************/
export async function getCronjobs() {
  return await axios.get("/cronjob");
}

export async function getCronjobByID(id) {
  return await axios.get(`/cronjob/${id}`);
}
export const createCronjob = async (data) => {
  return await axios.post("/cronjob", data);
};

export const updateCronjob = async (id, data) => {
  return await axios.put(`/cronjob/${id}`, data);
};
export const deleteCronjob = async (id) => {
  return await axios.delete(`/cronjob/${id}`);
};

/*************** Workflows ***************/
export async function getWorkflows() {
  return await axios.get("/workflow");
}

export async function getWorkflowByID(id) {
  return await axios.get(`/workflow/${id}`);
}
export const createWorkflow = async (data) => {
  return await axios.post("/workflow", data);
};

export const updateWorkflow = async (id, data) => {
  return await axios.put(`/workflow/${id}`, data);
};

export const deleteWorkflow = async (id) => {
  return await axios.delete(`/workflow/${id}`);
};
