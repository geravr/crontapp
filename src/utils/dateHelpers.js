export const getCurrentDate = (format) => {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, "0");
  const mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  const yyyy = today.getFullYear();

  switch (format) {
    case "yyyy-mm-dd":
      return `${yyyy}-${mm}-${dd}`;

    case "yyyy-mm":
      return `${yyyy}-${mm}`;

    case "yyyy":
      return `${yyyy}`;

    default:
      return `${dd}-${mm}-${yyyy}`;
  }
};

export const getNextYearsInArray = (years) => {
  let yearsArray = [];
  const currentYear = parseInt(getCurrentDate("yyyy"));
  for (let index = 0; index < years; index++) {
    yearsArray.push(`${currentYear + index}`);
  }
  return yearsArray;
};
