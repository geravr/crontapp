import { render, screen, waitFor } from "@testing-library/react";
import userEvent from '@testing-library/user-event';
import cronstrue from "cronstrue";

// Component
import Home, { getServerSideProps } from "@pages/index";

// Responses
import cronjobResponses from '@responses/cronjobs';
const { get: cronjobs } = cronjobResponses;

jest.mock("next/router", () => ({
  useRouter() {
    return {
      route: "/",
      pathname: "",
      query: "",
      asPath: "",
    };
  },
}));

beforeEach(async () => {
  const { props } = await getServerSideProps();
  const cronjobs = Object.values(props.cronjobs);
  render(<Home cronjobs={cronjobs} />);
});

describe("Home: is mounted", () => {
  test("Should have a title 'Cronjob List'", () => {
    expect(
      screen.getByRole("heading", { name: "Cronjob List" })
    ).toBeInTheDocument();
  });

  test("Should have list of cronjobs'", async () => {
    cronjobs.map(cronjob => {
      const schedulingTranslated = cronstrue.toString(cronjob.scheduling)
      expect(screen.getByText(cronjob.name)).toBeInTheDocument();
      expect(screen.getByText(cronjob.description)).toBeInTheDocument();
      expect(screen.getByText(schedulingTranslated)).toBeInTheDocument();
    });

  });

  test('should have a button to add new cronjob and others buttons to edit one cronjob', () => {
    expect(screen.getByRole("button", { name: /Add cronjob/i})).toBeInTheDocument();
    expect(screen.getAllByTestId('edit-button')[0]).toBeInTheDocument();
  })
  
  
});

  describe('Home: when user clicked delete button of any cronjob', () => {
    beforeEach(() => {
      userEvent.click(screen.getAllByTestId('delete-button')[0]);
    })
    test('Should open a modal with a title and content about of delete cronjob', () => {
      expect(screen.getByText(/Do you want to delete this cron job/i)).toBeInTheDocument();
      expect(screen.getByText(/Once deleted, it cannot be recovered/i)).toBeInTheDocument();
    });

    test('Should have a button for cancel and other for confirm action', () => {
      expect(screen.getByRole("button", { name: /Cancel/i})).toBeInTheDocument();
      expect(screen.getByRole("button", { name: /Yes, delete/i})).toBeInTheDocument();
    });
    
    test('When user clicks on cancel, the modal closes, and the content should not be displayed', async () => {
      userEvent.click(screen.getByRole("button", { name: /Cancel/i}));
      await waitFor(() => {
        expect(screen.queryByText(/Do you want to delete this cron job/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Once deleted, it cannot be recovered/i)).not.toBeInTheDocument();
      })
    });
    
    test('When user clicks on confirm delete, the modal closes, and show a delete confirmation message', async () => {
      userEvent.click(screen.getByRole("button", { name: /Yes, delete/i}));

      // Pending to add feedback in this section
      // expect(await screen.findByText(/Cronjob deleted successfully/i)).toBeInTheDocument();
    });
    
  })
