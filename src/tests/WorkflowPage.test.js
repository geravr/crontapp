import { render, screen, waitFor } from "@testing-library/react";
import userEvent from '@testing-library/user-event';

// Component
import WorkflowPage, { getServerSideProps } from "@pages/workflow/index";

// Responses
import workflowResponses from '@responses/workflows';
const { get: workflows } = workflowResponses;

jest.mock("next/router", () => ({
  useRouter() {
    return {
      route: "/",
      pathname: "",
      query: "",
      asPath: "",
    };
  },
}));

beforeEach(async () => {
  const { props } = await getServerSideProps();
  const workflows = Object.values(props.workflows);
  render(<WorkflowPage workflows={workflows} />);
});

describe("WorkflowPage: is mounted", () => {
  test("Should have a title 'Workflow List'", () => {
    expect(
      screen.getByRole("heading", { name: "Workflow List" })
    ).toBeInTheDocument();
  });

  test("Should have list of workflows'", async () => {
    workflows.map(workflow => {
      expect(screen.getByText(workflow.name)).toBeInTheDocument();
      expect(screen.getByText(workflow.description)).toBeInTheDocument();
    });

  });

  test('should have a button to add new workflow and others buttons to edit one workflow', () => {
    expect(screen.getByRole("button", { name: /Add workflow/i})).toBeInTheDocument();
    expect(screen.getAllByTestId('edit-button')[0]).toBeInTheDocument();
  })
  
  
});

  describe('WorkflowPage: when user clicked delete button of any workflow', () => {
    beforeEach(() => {
      userEvent.click(screen.getAllByTestId('delete-button')[0]);
    })
    test('Should open a modal with a title and content about of delete workflow', () => {
      expect(screen.getByText(/Do you want to delete this workflow/i)).toBeInTheDocument();
      expect(screen.getByText(/Once deleted, it cannot be recovered/i)).toBeInTheDocument();
    });

    test('Should have a button for cancel and other for confirm action', () => {
      expect(screen.getByRole("button", { name: /Cancel/i})).toBeInTheDocument();
      expect(screen.getByRole("button", { name: /Yes, delete/i})).toBeInTheDocument();
    });
    
    test('When user clicks on cancel, the modal closes, and the content should not be displayed', async () => {
      userEvent.click(screen.getByRole("button", { name: /Cancel/i}));
      await waitFor(() => {
        expect(screen.queryByText(/Do you want to delete this cron job/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Once deleted, it cannot be recovered/i)).not.toBeInTheDocument();
      })
    });
    
    test('When user clicks on confirm delete, the modal closes, and show a delete confirmation message', async () => {
      userEvent.click(screen.getByRole("button", { name: /Yes, delete/i}));

      // Pending to add feedback in this section
      // expect(await screen.findByText(/Workflow deleted successfully/i)).toBeInTheDocument();
    });
    
  })
