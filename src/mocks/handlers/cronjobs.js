import { rest } from 'msw';
import cronjobResponses from '@responses/cronjobs';

export const cronjobHandlers = [
  rest.get(`*/cronjob`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...cronjobResponses.get
      })
      );
  }),
  rest.get(`*/cronjob/[id]`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...cronjobResponses.getDetail,
      })
    );
  }),
  rest.delete(`*/cronjob/[id]`, (req, res, ctx) => {
    return res(
      ctx.status(200),
    );
  }),
];
