import { cronjobHandlers } from './cronjobs';
import { workflowHandlers } from './workflows';

const handlers = [
  ...cronjobHandlers,
  ...workflowHandlers,
];

export default handlers;
