import { rest } from 'msw';
import workflowResponses from '@responses/workflows';

export const workflowHandlers = [
  rest.get(`*/workflow`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...workflowResponses.get
      })
      );
  }),
  rest.get(`*/workflow/[id]`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        ...workflowResponses.getDetail,
      })
    );
  }),
  rest.delete(`*/workflow/[id]`, (req, res, ctx) => {
    return res(
      ctx.status(200),
    );
  }),
];
