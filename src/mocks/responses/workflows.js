const workflowResponses = {
  get: [
    {
      id: 1,
      name: "Workflow 1",
      description: "Description 1",
      created_at: "2021-03-21T20:18:47.000000Z",
      updated_at: "2021-04-12T19:24:58.000000Z",
      deleted_at: null,
    },
    {
      id: 5,
      name: "Workflow 2",
      description: "Description 2",
      created_at: "2021-03-21T21:40:33.000000Z",
      updated_at: "2021-03-21T21:40:33.000000Z",
      deleted_at: null,
    },
  ],
  getDetail: {
    id: 1,
    name: "Workflow 1",
    description: "Description 1",
    created_at: "2021-03-21T20:18:47.000000Z",
    updated_at: "2021-04-12T19:24:58.000000Z",
    deleted_at: null,
  },
};

export default workflowResponses;
