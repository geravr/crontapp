const cronjobResponses = {
  /************** Cronjobs ***************/
  get: [
    {
      id: 78,
      name: "Month test",
      description: "description 1",
      scheduling: "0-10 8/1 0,2,4 5/1 JAN,FEB,MAR,AUG ? *",
      workflow_id: 12,
      oauth_client_id: 6,
      created_at: "2021-04-20T22:54:35.000000Z",
      updated_at: "2021-04-20T22:54:35.000000Z",
      deleted_at: null,
    },
    {
      id: 79,
      name: "Test Year",
      description: "description 2",
      scheduling: "0,37 5/1 2-7 21/1 1-5 ? 2031/3",
      workflow_id: 8,
      oauth_client_id: 6,
      created_at: "2021-04-20T23:12:03.000000Z",
      updated_at: "2021-04-20T23:12:03.000000Z",
      deleted_at: null,
    },
  ],
  getDetail: {
    id: 78,
    name: "Month test",
    description: "desc",
    scheduling: "0-10 0 0,2,4 5/1 JAN,FEB,MAR,AUG ? *",
    workflow_id: 12,
    oauth_client_id: 6,
    created_at: "2021-04-20T22:54:35.000000Z",
    updated_at: "2021-04-21T15:20:10.000000Z",
    deleted_at: null,
  },
};

export default cronjobResponses;
