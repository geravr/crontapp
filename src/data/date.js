export const weekDays = [
  {
    number: "1",
    name: "Sunday",
    code: "SUN",
    lastDayCode: "1L",
    ordinalNumber: "1st"
  },
  {
    number: "2",
    name: "Monday",
    code: "MON",
    lastDayCode: "2L",
    ordinalNumber: "2nd"
  },
  {
    number: "3",
    name: "Tuesday",
    code: "TUE",
    lastDayCode: "3L",
    ordinalNumber: "3rd"
  },
  {
    number: "4",
    name: "Wednesday",
    code: "WED",
    lastDayCode: "4L",
    ordinalNumber: "4th"
  },
  {
    number: "5",
    name: "Thursday",
    code: "THU",
    lastDayCode: "5L",
    ordinalNumber: "5th"
  },
  {
    number: "6",
    name: "Friday",
    code: "FRI",
    lastDayCode: "6L",
    ordinalNumber: "6th"
  },
  {
    number: "7",
    name: "Saturday",
    code: "SAT",
    lastDayCode: "7L",
    ordinalNumber: "7th"
  },
];

export const monthDays = [
  {
    number: "1",
    ordinalNumber: "1st",
    lastDayCode: `L-1`,
    weekDayCode: "1W",
  },
  {
    number: "2",
    ordinalNumber: "2nd",
    lastDayCode: `L-2`,
    weekDayCode: "2W",
  },
  {
    number: "3",
    ordinalNumber: "3rd",
    lastDayCode: `L-3`,
    weekDayCode: "3W",
  },
  {
    number: "4",
    ordinalNumber: "4th",
    lastDayCode: `L-4`,
    weekDayCode: "4W",
  },
  {
    number: "5",
    ordinalNumber: "5th",
    lastDayCode: `L-5`,
    weekDayCode: "5W",
  },
  {
    number: "6",
    ordinalNumber: "6th",
    lastDayCode: `L-6`,
    weekDayCode: "6W",
  },
  {
    number: "7",
    ordinalNumber: "7th",
    lastDayCode: `L-7`,
    weekDayCode: "7W",
  },
  {
    number: "8",
    ordinalNumber: "8th",
    lastDayCode: `L-8`,
    weekDayCode: "8W",
  },
  {
    number: "9",
    ordinalNumber: "9th",
    lastDayCode: `L-9`,
    weekDayCode: "9W",
  },
  {
    number: "10",
    ordinalNumber: "10th",
    lastDayCode: `L-10`,
    weekDayCode: "10W",
  },
  {
    number: "11",
    ordinalNumber: "11th",
    lastDayCode: `L-11`,
    weekDayCode: "11W",
  },
  {
    number: "12",
    ordinalNumber: "12th",
    lastDayCode: `L-12`,
    weekDayCode: "12W",
  },
  {
    number: "13",
    ordinalNumber: "13th",
    lastDayCode: `L-13`,
    weekDayCode: "13W",
  },
  {
    number: "14",
    ordinalNumber: "14th",
    lastDayCode: `L-14`,
    weekDayCode: "14W",
  },
  {
    number: "15",
    ordinalNumber: "15th",
    lastDayCode: `L-15`,
    weekDayCode: "15W",
  },
  {
    number: "16",
    ordinalNumber: "16th",
    lastDayCode: `L-16`,
    weekDayCode: "16W",
  },
  {
    number: "17",
    ordinalNumber: "17th",
    lastDayCode: `L-17`,
    weekDayCode: "17W",
  },
  {
    number: "18",
    ordinalNumber: "18th",
    lastDayCode: `L-18`,
    weekDayCode: "18W",
  },
  {
    number: "19",
    ordinalNumber: "19th",
    lastDayCode: `L-19`,
    weekDayCode: "19W",
  },
  {
    number: "20",
    ordinalNumber: "20th",
    lastDayCode: `L-20`,
    weekDayCode: "20W",
  },
  {
    number: "21",
    ordinalNumber: "21st",
    lastDayCode: `L-21`,
    weekDayCode: "21W",
  },
  {
    number: "22",
    ordinalNumber: "22nd",
    lastDayCode: `L-22`,
    weekDayCode: "22W",
  },
  {
    number: "23",
    ordinalNumber: "23rd",
    lastDayCode: `L-23`,
    weekDayCode: "23W",
  },
  {
    number: "24",
    ordinalNumber: "24th",
    lastDayCode: `L-24`,
    weekDayCode: "24W",
  },
  {
    number: "25",
    ordinalNumber: "25th",
    lastDayCode: `L-25`,
    weekDayCode: "25W",
  },
  {
    number: "26",
    ordinalNumber: "26th",
    lastDayCode: `L-26`,
    weekDayCode: "26W",
  },
  {
    number: "27",
    ordinalNumber: "27th",
    lastDayCode: `L-27`,
    weekDayCode: "27W",
  },
  {
    number: "28",
    ordinalNumber: "28th",
    lastDayCode: `L-28`,
    weekDayCode: "28W",
  },
  {
    number: "29",
    ordinalNumber: "29th",
    lastDayCode: `L-29`,
    weekDayCode: "29W",
  },
  {
    number: "30",
    ordinalNumber: "30th",
    lastDayCode: `L-30`,
    weekDayCode: "30W",
  },
  {
    number: "31",
    ordinalNumber: "31st",
    lastDayCode: `L-31`,
    weekDayCode: "31W",
  },
];

export const monthsOfYear = [
  {
    number: "1",
    name: "January",
    code: "JAN",
  },
  {
    number: "2",
    name: "February",
    code: "FEB",
  },
  {
    number: "3",
    name: "March",
    code: "MAR",
  },
  {
    number: "4",
    name: "April",
    code: "APR",
  },
  {
    number: "5",
    name: "May",
    code: "MAY",
  },
  {
    number: "6",
    name: "June",
    code: "JUN",
  },
  {
    number: "7",
    name: "July",
    code: "JUL",
  },
  {
    number: "8",
    name: "August",
    code: "AUG",
  },
  {
    number: "9",
    name: "September",
    code: "SEP",
  },
  {
    number: "10",
    name: "October",
    code: "OCT",
  },
  {
    number: "11",
    name: "November",
    code: "NOV",
  },
  {
    number: "12",
    name: "December",
    code: "DEC",
  },
];