export const steps = [
  {
    id: 1,
    name: "Get started",
  },
  {
    id: 2,
    name: "Seconds",
  },
  {
    id: 3,
    name: "Minutes",
  },
  {
    id: 4,
    name: "Hours",
  },
  {
    id: 5,
    name: "Day",
  },
  {
    id: 6,
    name: "Month",
  },
  {
    id: 7,
    name: "Year",
  },
];